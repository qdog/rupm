use clipboard::ClipboardContext;
use clipboard::ClipboardProvider;

use std::collections::HashMap;
use std::io::Write;
use std::process::exit;
use std::process::{Command, Stdio};
use std::{thread, time::Duration};

use nix::unistd::{fork, ForkResult};

fn copy_to_clipboard(x: String, duration: Duration) {
    match unsafe { fork() } {
        Ok(ForkResult::Parent { .. }) => {}
        Ok(ForkResult::Child) => {
            let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();
            ctx.set_contents(x).unwrap();
            thread::sleep(duration);
            exit(0);
        }
        Err(e) => println!("the copying failed: {:?}", e),
    }
}

#[derive(Debug)]
struct VaultEntry {
    id: String,
    username: String,
    url: String,
}

struct Vault {
    entry_list: Vec<VaultEntry>,
}

impl Vault {
    fn new() -> Self {
        let output = Command::new("rbw")
            .arg("ls")
            .args(["--fields", "id", "user", "name"])
            .output()
            .unwrap()
            .stdout;
        let entry_list = String::from_utf8_lossy(&output);
        let mut vault_entries = Vec::new();
        for element in entry_list.lines() {
            let v = element.split('\t').collect::<Vec<&str>>();
            let e = VaultEntry {
                id: v[0].to_string(),
                username: v[1].to_string(),
                url: v[2].to_string(),
            };
            vault_entries.push(e);
        }
        Self {
            entry_list: vault_entries,
        }
    }

    fn get_entries(&self) -> Vec<&VaultEntry> {
        let mut l: Vec<&VaultEntry> = Vec::new();
        for e in &self.entry_list {
            l.push(e);
        }
        l
    }

    fn get_secret(&self, entry: &VaultEntry) -> String {
        let secrety = Command::new("rbw")
            .arg("get")
            .arg(&entry.id)
            .output()
            .unwrap()
            .stdout;
        let secret_string = String::from_utf8_lossy(&secrety)
            .to_string()
            .strip_suffix('\n')
            .unwrap()
            .to_string();
        secret_string
    }

    fn unlock(&self) -> bool {
        let status = Command::new("rbw").arg("unlock").status().unwrap();
        status.success()
    }
}

struct Menu<'a> {
    option_map: HashMap<String, &'a VaultEntry>,
}

impl<'a> Menu<'a> {
    fn new() -> Self {
        Self {
            option_map: HashMap::new(),
        }
    }

    fn choose(&mut self, entries: Vec<&'a VaultEntry>) -> Option<&VaultEntry> {
        self.load_vecve(entries);
        let mut all_names = self.option_map.keys().cloned().collect::<Vec<String>>();
        all_names.sort();
        let all_names_joined = all_names.join("\n");

        let puty = Command::new("rofi")
            .arg("-dmenu")
            .arg("-i")
            .args(["-p", "RustyBitwarden"])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();
        write!(puty.stdin.as_ref().unwrap(), "{}", all_names_joined).unwrap();
        let oo = puty.wait_with_output().unwrap();
        if oo.status.success() {
            let mut res = oo.stdout;
            res.pop(); // to remove the last char which is a newline
            let chosen_entry = String::from_utf8(res).unwrap();
            Some(self.option_map.get(&chosen_entry).unwrap()) //[&chosen_entry]
        } else {
            None
        }
    }

    fn load_vecve(&mut self, entries: Vec<&'a VaultEntry>) {
        for e in entries {
            let name = format!("{} - {}", e.url, e.username);
            self.option_map.insert(name, e);
        }
    }
}

fn better_cmd() {
    let v = Vault::new();
    let mut m = Menu::new();
    let is_unlocked = v.unlock();
    if !(is_unlocked) {
        return;
    }
    let c_o = m.choose(v.get_entries());
    if let Some(o) = c_o {
        let s = v.get_secret(o);
        copy_to_clipboard(s, Duration::from_secs(10));
    }
}

fn main() {
    better_cmd();
}
